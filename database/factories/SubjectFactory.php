<?php

use Faker\Generator as Faker;

$factory->define(App\Subject::class, function (Faker $faker) {
    return [
        'code' => $faker->name(),
        'arabic_name' => $faker->name(),
        'english_name' => $faker->name(),
        'short_name' => $faker->name(),
        'units' => $faker->randomDigitNotNull(),
        'active' => $faker->boolean(),
    ];
});
