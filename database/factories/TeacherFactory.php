<?php

use Faker\Generator as Faker;

$factory->define(App\Teacher::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'phone' => $faker->phoneNumber(),
        'email' => $faker->safeEmail(),
        'active' => $faker->boolean(),
        'gender_id' => function () {
            return factory(App\Gender::class)->create()->id;
        },
        'specialty_id' => function () {
            return factory(App\Specialty::class)->create()->id;
        },
        'nationality_id' => function () {
            return factory(App\Nationality::class)->create()->id;
        },
    ];
});
