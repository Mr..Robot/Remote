<?php

use Faker\Generator as Faker;

$factory->define(App\Student::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name(),
        'second_name' => $faker->name(),
        'last_name' => $faker->name(),
        'contract_number' => $faker->name(),
        'mother_full_name' => $faker->name(),
        'active' => $faker->boolean(),
        'date_birth' => $faker->date(),
        'phone' => $faker->phoneNumber(),
        'email' => $faker->safeEmail(),
        'national_number' => $faker->randomDigitNotNull(),
        'gender_id' => function () {
            return factory(App\Gender::class)->create()->id;
        },
        'nationality_id' => function () {
            return factory(App\Nationality::class)->create()->id;
        },
    ];
});
