<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(StudentTableSeeder::class);
        $this->call(TeacherTableSeeder::class);
        $this->call(SubjectTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        $this->call(SpecialtyTableSeeder::class);
       // $this->call(GenderTableSeeder::class);
        $this->call(NationalityTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
    }
}
