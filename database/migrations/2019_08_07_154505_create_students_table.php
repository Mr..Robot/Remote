<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('second_name');
            $table->string('last_name');
            $table->string('contract_number');
            $table->string('mother_full_name');
            $table->boolean('active');
            $table->date('date_birth');
            $table->string('phone');
            $table->string('email');
            $table->integer('national_number')->nullable();
            $table->integer('gender_id')->unsigned()->index();
            $table->integer('nationality_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('gender_id')->references('id')->on('genders');
            $table->foreign('nationality_id')->references('id')->on('nationalities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
