<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->boolean('active');
            $table->integer('gender_id')->unsigned()->index();
            $table->integer('specialty_id')->unsigned()->index();
            $table->integer('nationality_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('gender_id')->references('id')->on('genders');
            $table->foreign('specialty_id')->references('id')->on('specialties');
            $table->foreign('nationality_id')->references('id')->on('nationalities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
