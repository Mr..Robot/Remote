<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'arabic_name', 'english_name', 'short_name', 'units', 'active'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'code' => 'required|string',
            'arabic_name' => 'required|string',
            'english_name' => 'required|string',
            'short_name' => 'required|string',
            'units' => 'required|numeric',
            'active' => 'required|boolean',
        ];
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::paginate(10);
    }
}
