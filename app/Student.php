<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'second_name', 'last_name', 'contract_number', 'mother_full_name', 'active', 'date_birth', 'phone', 'email', 'national_number', 'gender_id', 'nationality_id'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'first_name' => 'required|string|min:2|max:20',
            'second_name' => 'required|string',
            'last_name' => 'required|string',
            'contract_number' => 'required|string',
            'mother_full_name' => 'required|string',
            'active' => 'required|boolean',
            'date_birth' => 'required|date',
            'phone' => 'required|string',
            'email' => 'required|email',
            'national_number' => 'nullable|numeric',
            'gender_id' => 'required|numeric|exists:genders,id',
            'nationality_id' => 'required|numeric|exists:nationalities,id',
        ];
    }

    /**
     * Get the gender for the Student.
     */
    public function gender()
    {
        return $this->belongsTo('App\Gender');
    }

    /**
     * Get the nationality for the Student.
     */
    public function nationality()
    {
        return $this->belongsTo('App\Nationality');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['gender', 'nationality'])->paginate(2);
    }
}
