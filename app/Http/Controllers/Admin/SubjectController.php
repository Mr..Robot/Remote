<?php

namespace App\Http\Controllers\Admin;

use App\Subject;
use App\Http\Controllers\Controller;

class SubjectController extends Controller
{
    /**
     * Display a list of Subjects.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::getList();

        return view('admin.subjects.index', compact('subjects'));
    }

    /**
     * Show the form for creating a new Subject
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subjects.add');
    }

    /**
     * Save new Subject
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Subject::validationRules());

        $subject = Subject::create($validatedData);

        return redirect()->route('admin.subjects.index')->with([
            'type' => 'success',
            'message' => 'Subject added'
        ]);
    }

    /**
     * Show the form for editing the specified Subject
     *
     * @param \App\Subject $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        return view('admin.subjects.edit', compact('subject'));
    }

    /**
     * Update the Subject
     *
     * @param \App\Subject $subject
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Subject $subject)
    {
        $validatedData = request()->validate(
            Subject::validationRules($subject->id)
        );

        $subject->update($validatedData);

        return redirect()->route('admin.subjects.index')->with([
            'type' => 'success',
            'message' => 'Subject Updated'
        ]);
    }

    /**
     * Delete the Subject
     *
     * @param \App\Subject $subject
     * @return void
     */
    public function destroy(Subject $subject)
    {
        $subject->delete();

        return redirect()->route('admin.subjects.index')->with([
            'type' => 'success',
            'message' => 'Subject deleted successfully'
        ]);
    }
}
