<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'email', 'active', 'gender_id', 'specialty_id', 'nationality_id'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'name' => 'required|string',
            'phone' => 'required|string',
            'email' => 'required|email',
            'active' => 'required|boolean',
            'gender_id' => 'required|numeric|exists:genders,id',
            'specialty_id' => 'required|numeric|exists:specialties,id',
            'nationality_id' => 'required|numeric|exists:nationalities,id',
        ];
    }

    /**
     * Get the gender for the Teacher.
     */
    public function gender()
    {
        return $this->belongsTo('App\Gender');
    }

    /**
     * Get the specialty for the Teacher.
     */
    public function specialty()
    {
        return $this->belongsTo('App\Specialty');
    }

    /**
     * Get the nationality for the Teacher.
     */
    public function nationality()
    {
        return $this->belongsTo('App\Nationality');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['gender', 'specialty', 'nationality'])->paginate(10);
    }
}
