@extends('admin.layouts.app', ['page' => 'student'])

@section('title', 'Students')

@section('content')
<div class="x_title">
    <h2>Students</h2>

    <a class="pull-right btn btn-primary"
        href="{{ route('admin.students.create') }}"
    >
        Add New
    </a>

    <div class="clearfix"></div>
</div>

<br>

<div class="box-body">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Second Name</th>
            <th>Last Name</th>
            <th>Contract Number</th>
            <th>Gender</th>
            <th>Action</th>
        </tr>

        @forelse ($students as $student)
            <tr>
                <td>{{ $student->id }}</td>
                <td>{{ $student->first_name }}</td>
                <td>{{ $student->second_name }}</td>
                <td>{{ $student->last_name }}</td>
                <td>{{ $student->contract_number }}</td>
                <td>{{ $student->gender->name }}</td>
                <td>
                    <a href="{{ route('admin.students.edit', ['student' => $student->id]) }}">
                        <i class="fa fa-pencil-square-o"></i>
                    </a>

                    <form action="{{ route('admin.students.destroy', ['student' => $student->id]) }}"
                        method="POST"
                        class="inline pointer"
                    >
                        @csrf
                        @method('DELETE')

                        <a onclick="if (confirm('Are you sure?')) { this.parentNode.submit() }">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="7">No records found</td>
            </tr>
        @endforelse
    </table>
</div>

<div class="box-footer clearfix">
    {{ $students->links('vendor.pagination.default') }}
</div>
@endsection
