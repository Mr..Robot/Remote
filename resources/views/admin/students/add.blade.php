@extends('admin.layouts.app', ['page' => 'student'])

@section('title', 'Add New Student')

@section('content')

<div class="x_title">
    <h2>Add New Student</h2>

    <div class="clearfix"></div>
</div>

<br>

<form role="form" method="POST" action="{{ route('admin.students.store') }}">
    @csrf

    <div class="box-body">
        <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text"
                class="form-control"
                name="first_name"
                required
                placeholder="First Name"
                value="{{ old('first_name') }}"
                id="first_name"
            >
        </div>

        <div class="form-group">
            <label for="second_name">Second Name</label>
            <input type="text"
                class="form-control"
                name="second_name"
                required
                placeholder="Second Name"
                value="{{ old('second_name') }}"
                id="second_name"
            >
        </div>

        <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text"
                class="form-control"
                name="last_name"
                required
                placeholder="Last Name"
                value="{{ old('last_name') }}"
                id="last_name"
            >
        </div>

        <div class="form-group">
            <label for="contract_number">Contract Number</label>
            <input type="text"
                class="form-control"
                name="contract_number"
                required
                placeholder="Contract Number"
                value="{{ old('contract_number') }}"
                id="contract_number"
            >
        </div>

        <div class="form-group">
            <label for="mother_full_name">Mother Full Name</label>
            <input type="text"
                class="form-control"
                name="mother_full_name"
                required
                placeholder="Mother Full Name"
                value="{{ old('mother_full_name') }}"
                id="mother_full_name"
            >
        </div>

        <div class="form-group">
            <label class="checkbox-inline">
                <input type="hidden" name="active" value="0">
                <input type="checkbox"
                    name="active"
                    value="1"
                    {{ old('active') == 1 ? 'checked' : '' }}
                >
                    Active
            </label>
        </div>

        <div class="form-group">
            <label for="date_birth">Date Birth</label>
            <input type="date"
                class="form-control"
                name="date_birth"
                required
                placeholder="Date Birth"
                value="{{ old('date_birth') }}"
                id="date_birth"
            >
        </div>

        <div class="form-group">
            <label for="phone">Phone</label>
            <input type="text"
                class="form-control"
                name="phone"
                required
                placeholder="Phone"
                value="{{ old('phone') }}"
                id="phone"
            >
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input type="email"
                class="form-control"
                name="email"
                required
                placeholder="Email"
                value="{{ old('email') }}"
                id="email"
            >
        </div>

        <div class="form-group">
            <label for="national_number">National Number</label>
            <input type="number"
                class="form-control"
                name="national_number"
                required
                placeholder="National Number"
                value="{{ old('national_number') }}"
                step="any"
                id="national_number"
            >
        </div>

        <div class="form-group">
            <label for="gender-id">Gender</label>
            <select class="form-control"
                name="gender_id"
                required
                id="gender-id"
            >
                @foreach ($genders as $gender)
                    <option value="{{ $gender->id }}"
                        {{ old('gender_id') == $gender->id ? 'selected' : '' }}
                    >
                        {{ $gender->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="nationality-id">Nationality</label>
            <select class="form-control"
                name="nationality_id"
                required
                id="nationality-id"
            >
                @foreach ($nationalities as $nationality)
                    <option value="{{ $nationality->id }}"
                        {{ old('nationality_id') == $nationality->id ? 'selected' : '' }}
                    >
                        {{ $nationality->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>

        <a href="{{ route('admin.students.index') }}" class="btn btn-default">
            Cancel
        </a>
    </div>
</form>
@endsection
