@extends('admin.layouts.app', ['page' => 'subject'])

@section('title', 'Subjects')

@section('content')
<div class="x_title">
    <h2>Subjects</h2>

    <a class="pull-right btn btn-primary"
        href="{{ route('admin.subjects.create') }}"
    >
        Add New
    </a>

    <div class="clearfix"></div>
</div>

<br>

<div class="box-body">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Code</th>
            <th>Arabic Name</th>
            <th>English Name</th>
            <th>Short Name</th>
            <th>Action</th>
        </tr>

        @forelse ($subjects as $subject)
            <tr>
                <td>{{ $subject->id }}</td>
                <td>{{ $subject->code }}</td>
                <td>{{ $subject->arabic_name }}</td>
                <td>{{ $subject->english_name }}</td>
                <td>{{ $subject->short_name }}</td>
                <td>
                    <a href="{{ route('admin.subjects.edit', ['subject' => $subject->id]) }}">
                        <i class="fa fa-pencil-square-o"></i>
                    </a>

                    <form action="{{ route('admin.subjects.destroy', ['subject' => $subject->id]) }}"
                        method="POST"
                        class="inline pointer"
                    >
                        @csrf
                        @method('DELETE')

                        <a onclick="if (confirm('Are you sure?')) { this.parentNode.submit() }">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6">No records found</td>
            </tr>
        @endforelse
    </table>
</div>

<div class="box-footer clearfix">
    {{ $subjects->links('vendor.pagination.default') }}
</div>
@endsection
