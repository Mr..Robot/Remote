@extends('admin.layouts.app', ['page' => 'subject'])

@section('title', 'Add New Subject')

@section('content')

<div class="x_title">
    <h2>Add New Subject</h2>

    <div class="clearfix"></div>
</div>

<br>

<form role="form" method="POST" action="{{ route('admin.subjects.store') }}">
    @csrf

    <div class="box-body">
        <div class="form-group">
            <label for="code">Code</label>
            <input type="text"
                class="form-control"
                name="code"
                required
                placeholder="Code"
                value="{{ old('code') }}"
                id="code"
            >
        </div>

        <div class="form-group">
            <label for="arabic_name">Arabic Name</label>
            <input type="text"
                class="form-control"
                name="arabic_name"
                required
                placeholder="Arabic Name"
                value="{{ old('arabic_name') }}"
                id="arabic_name"
            >
        </div>

        <div class="form-group">
            <label for="english_name">English Name</label>
            <input type="text"
                class="form-control"
                name="english_name"
                required
                placeholder="English Name"
                value="{{ old('english_name') }}"
                id="english_name"
            >
        </div>

        <div class="form-group">
            <label for="short_name">Short Name</label>
            <input type="text"
                class="form-control"
                name="short_name"
                required
                placeholder="Short Name"
                value="{{ old('short_name') }}"
                id="short_name"
            >
        </div>

        <div class="form-group">
            <label for="units">Units</label>
            <input type="number"
                class="form-control"
                name="units"
                required
                placeholder="Units"
                value="{{ old('units') }}"
                step="any"
                id="units"
            >
        </div>

        <div class="form-group">
            <label class="checkbox-inline">
                <input type="hidden" name="active" value="0">
                <input type="checkbox"
                    name="active"
                    value="1"
                    {{ old('active') == 1 ? 'checked' : '' }}
                >
                    Active
            </label>
        </div>
    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>

        <a href="{{ route('admin.subjects.index') }}" class="btn btn-default">
            Cancel
        </a>
    </div>
</form>
@endsection
