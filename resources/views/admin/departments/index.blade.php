@extends('admin.layouts.app', ['page' => 'department'])

@section('title', 'Departments')

@section('content')
<div class="x_title">
    <h2>Departments</h2>

    <a class="pull-right btn btn-primary"
        href="{{ route('admin.departments.create') }}"
    >
        Add New
    </a>

    <div class="clearfix"></div>
</div>

<br>

<div class="box-body">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Action</th>
        </tr>

        @forelse ($departments as $department)
            <tr>
                <td>{{ $department->id }}</td>
                <td>{{ $department->name }}</td>
                <td>
                    <a href="{{ route('admin.departments.edit', ['department' => $department->id]) }}">
                        <i class="fa fa-pencil-square-o"></i>
                    </a>

                    <form action="{{ route('admin.departments.destroy', ['department' => $department->id]) }}"
                        method="POST"
                        class="inline pointer"
                    >
                        @csrf
                        @method('DELETE')

                        <a onclick="if (confirm('Are you sure?')) { this.parentNode.submit() }">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">No records found</td>
            </tr>
        @endforelse
    </table>
</div>

<div class="box-footer clearfix">
    {{ $departments->links('vendor.pagination.default') }}
</div>
@endsection
