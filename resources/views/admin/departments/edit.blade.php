@extends('admin.layouts.app', ['page' => 'department'])

@section('title', 'Edit Department')

@section('content')
<div class="x_title">
    <h2>Edit Department</h2>

    <div class="clearfix"></div>
</div>

<form role="form" method="POST" action="{{ route('admin.departments.update', ['department' => $department->id]) }}">
    @csrf
    @method('PUT')

    <div class="box-body">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text"
                class="form-control"
                name="name"
                required
                placeholder="Name"
                value="{{ old('name', $department->name) }}"
                id="name"
            >
        </div>
    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Update</button>

        <a href="{{ route('admin.departments.index') }}" class="btn btn-default">
            Cancel
        </a>
    </div>
</form>
@endsection
