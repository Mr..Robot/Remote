@extends('admin.layouts.app', ['page' => 'teacher'])

@section('title', 'Add New Teacher')

@section('content')

<div class="x_title">
    <h2>Add New Teacher</h2>

    <div class="clearfix"></div>
</div>

<br>

<form role="form" method="POST" action="{{ route('admin.teachers.store') }}">
    @csrf

    <div class="box-body">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text"
                class="form-control"
                name="name"
                required
                placeholder="Name"
                value="{{ old('name') }}"
                id="name"
            >
        </div>

        <div class="form-group">
            <label for="phone">Phone</label>
            <input type="text"
                class="form-control"
                name="phone"
                required
                placeholder="Phone"
                value="{{ old('phone') }}"
                id="phone"
            >
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input type="email"
                class="form-control"
                name="email"
                required
                placeholder="Email"
                value="{{ old('email') }}"
                id="email"
            >
        </div>

        <div class="form-group">
            <label class="checkbox-inline">
                <input type="hidden" name="active" value="0">
                <input type="checkbox"
                    name="active"
                    value="1"
                    {{ old('active') == 1 ? 'checked' : '' }}
                >
                    Active
            </label>
        </div>

        <div class="form-group">
            <label for="gender-id">Gender</label>
            <select class="form-control"
                name="gender_id"
                required
                id="gender-id"
            >
                @foreach ($genders as $gender)
                    <option value="{{ $gender->id }}"
                        {{ old('gender_id') == $gender->id ? 'selected' : '' }}
                    >
                        {{ $gender->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="specialty-id">Specialty</label>
            <select class="form-control"
                name="specialty_id"
                required
                id="specialty-id"
            >
                @foreach ($specialties as $specialty)
                    <option value="{{ $specialty->id }}"
                        {{ old('specialty_id') == $specialty->id ? 'selected' : '' }}
                    >
                        {{ $specialty->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="nationality-id">Nationality</label>
            <select class="form-control"
                name="nationality_id"
                required
                id="nationality-id"
            >
                @foreach ($nationalities as $nationality)
                    <option value="{{ $nationality->id }}"
                        {{ old('nationality_id') == $nationality->id ? 'selected' : '' }}
                    >
                        {{ $nationality->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>

        <a href="{{ route('admin.teachers.index') }}" class="btn btn-default">
            Cancel
        </a>
    </div>
</form>
@endsection
