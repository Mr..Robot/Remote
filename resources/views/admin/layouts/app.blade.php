<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }} - @yield('title')</title>

    <link href="{{ mix('/css/admin/vendor.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/admin/app.css') }}" rel="stylesheet">

    {{-- You can put page wise internal css style in styles section --}}
    @stack('styles')
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            {{-- Sidebar --}}
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="{{ route('admin.dashboard') }}" class="site_title">
                            <i class="fa fa-paw"></i>
                            <span>{{ config('app.name') }}</span>
                        </a>
                    </div>

                    {{-- Menu profile quick info --}}
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="{{ asset('images/admin-avatar.png') }}" alt="Admin avatar" class="img-circle profile_img">
                        </div>

                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2>{{ Auth::guard('admin')->user()->name }}</h2>
                        </div>
                    </div>
                    <br>

                    {{-- Sidebar Menu --}}
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>Menu</h3>

                            <ul class="nav side-menu">
                                <li {{ $page == 'dashboard' ? ' class=active' : '' }}>
                                    <a href="{{ route('admin.dashboard') }}">
                                        <i class="fa fa-home"></i>
                                        Dashboard
                                    </a>
                                </li>

                                <li {{ $page == 'student' ? ' class=active' : '' }}>
                                    <a href="{{ route('admin.students.index') }}">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Students</span>
                                    </a>
                                </li>

                                <li {{ $page == 'teacher' ? ' class=active' : '' }}>
                                    <a href="{{ route('admin.teachers.index') }}">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Teachers</span>
                                    </a>
                                </li>

                                <li {{ $page == 'subject' ? ' class=active' : '' }}>
                                    <a href="{{ route('admin.subjects.index') }}">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Subjects</span>
                                    </a>
                                </li>

                                <li {{ $page == 'department' ? ' class=active' : '' }}>
                                    <a href="{{ route('admin.departments.index') }}">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Departments</span>
                                    </a>
                                </li>

                                <li {{ $page == 'specialty' ? ' class=active' : '' }}>
                                    <a href="{{ route('admin.specialties.index') }}">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Specialties</span>
                                    </a>
                                </li>

                                {{-- <li {{ $page == 'gender' ? ' class=active' : '' }}>
                                    <a href="{{ route('admin.genders.index') }}">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Genders</span>
                                    </a>
                                </li>

                                <li {{ $page == 'nationality' ? ' class=active' : '' }}>
                                    <a href="{{ route('admin.nationalities.index') }}">
                                        <i class="fa fa-arrow-right"></i>
                                        <span>Nationalities</span>
                                    </a>
                                </li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Profile Menu --}}
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle">
                                <i class="fa fa-bars"></i>
                            </a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="{{ asset('images/admin-avatar.png') }}" alt="Admin avatar">

                                    {{ Auth::guard('admin')->user()->name }}

                                    <span class="fa fa-angle-down"></span>
                                </a>

                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li>
                                        <a href="{{ route('admin.profile') }}">
                                            Profile
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('admin.logout') }}">
                                            <i class="fa fa-sign-out pull-right"></i>
                                            Log Out
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="clearfix"></div>

            {{--  Page Content  --}}
            <div class="right_col" role="main">
                <div class="row tile_count">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="row">
                                    @if ($errors->all())
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $message)
                                                <li>{{ $message }}</li>
                                            @endforeach
                                        </ul>
                                    @endif

                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            {{--  Footer  --}}
            <footer>
                <div class="pull-right hidden-xs">
                    Anything you want
                </div>

                <strong>
                    Created with
                    <i class="fa fa-heart"></i>
                    by
                    <a href="https://laravelfactory.com" target="_blank">Laravel Factory</a>.
                </strong>
                <div class="clearfix"></div>
            </footer>
        </div>
    </div>

    <script src="{{ mix('/js/admin/vendor.js') }}"></script>
    <script src="{{ mix('/js/admin/app.js') }}"></script>

    @if (session('message'))
        <script>
            showNotice("{{ session('type') }}", "{{ session('message') }}");
        </script>
    @endif

    {{-- You can put page wise javascript in scripts section --}}
    @stack('scripts')
</body>
</html>