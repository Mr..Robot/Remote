@extends('admin.layouts.app', ['page' => 'specialty'])

@section('title', 'Edit Specialty')

@section('content')
<div class="x_title">
    <h2>Edit Specialty</h2>

    <div class="clearfix"></div>
</div>

<form role="form" method="POST" action="{{ route('admin.specialties.update', ['specialty' => $specialty->id]) }}">
    @csrf
    @method('PUT')

    <div class="box-body">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text"
                class="form-control"
                name="name"
                required
                placeholder="Name"
                value="{{ old('name', $specialty->name) }}"
                id="name"
            >
        </div>
    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Update</button>

        <a href="{{ route('admin.specialties.index') }}" class="btn btn-default">
            Cancel
        </a>
    </div>
</form>
@endsection
