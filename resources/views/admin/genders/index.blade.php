@extends('admin.layouts.app', ['page' => 'gender'])

@section('title', 'Genders')

@section('content')
<div class="x_title">
    <h2>Genders</h2>

    <a class="pull-right btn btn-primary"
        href="{{ route('admin.genders.create') }}"
    >
        Add New
    </a>

    <div class="clearfix"></div>
</div>

<br>

<div class="box-body">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Action</th>
        </tr>

        @forelse ($genders as $gender)
            <tr>
                <td>{{ $gender->id }}</td>
                <td>{{ $gender->name }}</td>
                <td>
                    <a href="{{ route('admin.genders.edit', ['gender' => $gender->id]) }}">
                        <i class="fa fa-pencil-square-o"></i>
                    </a>

                    <form action="{{ route('admin.genders.destroy', ['gender' => $gender->id]) }}"
                        method="POST"
                        class="inline pointer"
                    >
                        @csrf
                        @method('DELETE')

                        <a onclick="if (confirm('Are you sure?')) { this.parentNode.submit() }">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">No records found</td>
            </tr>
        @endforelse
    </table>
</div>

<div class="box-footer clearfix">
    {{ $genders->links('vendor.pagination.default') }}
</div>
@endsection
